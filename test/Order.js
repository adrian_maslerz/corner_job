
//Require the dev-dependencies
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../bin/www');
const should = chai.should();
const config = require("./config");

chai.use(chaiHttp);

const endpoints = {
    addOrder: "/api/coffees/:id/orders",
    getOrders: "/api/orders"
};

let user = null;
describe('POST endpoint ' + endpoints.addOrder, () => {

    let coffee = null;
    //clearing orders adding coffee and getting user
    before(done => {
        const { User } = require("../app/models/User");
        User.findOne({ username: config.user.username })
            .then(userObject => {

                user = userObject;

                const { Coffee } = require("../app/models/Coffee");
                return Coffee.create({
                    name: config.coffee.name,
                    stock: config.coffee.stock,
                    price: config.coffee.price,
                    intensity: config.coffee.intensity
                })
            }).then(coffeeObject => {
                coffee = coffeeObject;

                const { Order } = require("../app/models/Order");
                return Order.remove()
            })
            .then(() => done());
    });

    //not found
    it('should reject adding because of not found coffee', (done) => {
        chai.request(server)
            .post(endpoints.addOrder.replace(/:id/, 1))
            .set({
                "X-access-token": user.auth.token
            })
            .send({
                quantity: config.order.quantity,
            })
            .end((error, res) => {

                res.should.have.status(404);
                res.body.should.be.a('object');
                res.body.should.have.property("errors").be.a("array");
                res.body.errors[0].should.have.property("field").eq("coffee");
                res.body.errors[0].should.have.property("message").be.a("string");

                done();
            });
    });

    //out of stock
    it('should reject adding because out of stock', (done) => {
        chai.request(server)
            .post(endpoints.addOrder.replace(/:id/, coffee._id.toString()))
            .set({
                "X-access-token": user.auth.token
            })
            .send({
                quantity: config.order.quantity + 100,
            })
            .end((error, res) => {

                res.should.have.status(409);
                res.body.should.be.a('object');
                res.body.should.have.property("errors").be.a("array");
                res.body.errors[0].should.have.property("field").eq("stock");
                res.body.errors[0].should.have.property("message").be.a("string");

                done();
            });
    });

    //successfully
    it('should add order successfully', (done) => {
        chai.request(server)
            .post(endpoints.addOrder.replace(/:id/, coffee._id.toString()))
            .set({
                "X-access-token": user.auth.token
            })
            .send({
                quantity: config.order.quantity,
            })
            .end((error, res) => {

                res.should.have.status(201);
                res.body.should.be.a('object');
                res.body.should.have.property("status").eq(true);

                done();
            });
    });
});

describe('GET endpoint ' + endpoints.getOrders, () => {

    //success
    it('should get paginated orders', (done) => {
        chai.request(server)
            .get(endpoints.getOrders)
            .set({
                "X-access-token": user.auth.token
            })
            .send({
                page: 1,
                results: 10
            })
            .end((error, res) => {

                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property("results").be.a("array");
                res.body.should.have.property("pages").be.a("number");
                res.body.should.have.property("total").be.a("number");
                res.body.results.should.not.be.empty;
                res.body.results[0].should.have.property("_id").be.a("string");
                res.body.results[0].should.have.property("quantity").be.a("number");
                res.body.results[0].should.have.property("created").be.a("number");
                res.body.results[0].should.have.property("amount").be.a("number");

                //user
                res.body.results[0].should.have.property("user").be.a("object");
                res.body.results[0].user.should.have.property("_id").be.a("string");
                res.body.results[0].user.should.have.property("username").be.a("string");

                //coffee
                res.body.results[0].should.have.property("coffee").be.a("object");
                res.body.results[0].coffee.should.have.property("name").be.a("string");
                res.body.results[0].coffee.should.have.property("intensity").be.a("number");
                res.body.results[0].coffee.should.have.property("price").be.a("number");
                res.body.results[0].coffee.should.have.property("stock").be.a("number");

                done();
            });
    });

});
