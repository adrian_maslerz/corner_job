
//Require the dev-dependencies
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../bin/www');
const should = chai.should();
const config = require("./config");

chai.use(chaiHttp);

const endpoints = {
    register: "/api/register",
    login: "/api/login",
};

//Register
describe('POST endpoint ' + endpoints.register, () => {

    //clear user
    before(done => {
        const { User } = require("../app/models/User");
        User.remove({ username: config.user.username }, (err) => {
            done();
        });
    });

    //wrong parameters
    it('should reject adding because of invalid password', (done) => {
        chai.request(server)
            .post(endpoints.register)
            .send({
                username: config.user.username,
                password: ""
            })
            .end((error, res) => {

                res.should.have.status(406);
                res.body.should.be.a('object');
                res.body.should.have.property("errors").be.a("array");
                res.body.errors[0].should.have.property("field").eq("password");
                res.body.errors[0].should.have.property("message").be.a("string");

                done();
            });
    });

    //added correctly
    it('should register new user', (done) => {
        chai.request(server)
            .post(endpoints.register)
            .send({
                username: config.user.username,
                password: config.user.password
            })
            .end((error, res) => {

                res.should.have.status(201);
                res.body.should.be.a('object');
                res.body.should.have.property("_id").be.a("string");
                res.body.should.have.property("username").be.a("string");
                res.body.should.have.property("auth").be.a("object");
                res.body.auth.should.have.property("token").be.a("string");
                res.body.auth.should.have.property("refresh_token").be.a("string");

                //clear added user
                done();
            });
    });
});

//Login
describe('POST endpoint ' + endpoints.login, () => {

    //wrong parameters
    it('should reject login because of wrong email or password', (done) => {
        chai.request(server)
            .post(endpoints.login)
            .send({
                username: config.user.username,
                password: ""
            })
            .end((error, res) => {

                res.should.have.status(401);
                res.body.should.be.a('object');
                res.body.should.have.property("errors").be.a("array");
                res.body.errors[0].should.have.property("field").eq("auth");
                res.body.errors[0].should.have.property("message").be.a("string");

                done();
            });
    });

    //added correctly
    it('should login user', (done) => {
        chai.request(server)
            .post(endpoints.login)
            .send({
                username: config.user.username,
                password: config.user.password
            })
            .end((error, res) => {

                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property("_id").be.a("string");
                res.body.should.have.property("username").be.a("string");
                res.body.should.have.property("auth").be.a("object");
                res.body.auth.should.have.property("token").be.a("string");
                res.body.auth.should.have.property("refresh_token").be.a("string");

                done();
            });
    });
});

