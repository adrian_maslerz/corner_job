
//Require the dev-dependencies
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../bin/www');
const should = chai.should();
const config = require("./config");

chai.use(chaiHttp);

const endpoints = {
    addCoffee: "/api/coffees",
    getCoffees: "/api/coffees",
    getCoffee: "/api/coffees/:id",
    updateCoffee: "/api/coffees/:id",
    deleteCoffee: "/api/coffees/:id",
};

let user = null;
describe('POST endpoint ' + endpoints.addCoffee, () => {

    //adding admin permissions and clearing test coffees
    before(done => {
        const { User } = require("../app/models/User");
        User.findOne({ username: config.user.username })
            .then(userObject => {

                user = userObject;
                user.roles.push("ADMIN");

                return user.save();
            })
            .then(() => {

                const { Coffee } = require("../app/models/Coffee");
                Coffee.remove({ name: config.coffee.name }, (err) => {
                    done();
                });
            });
    });

    //auth fail
    it('should reject adding because of unauthorized request', (done) => {
        chai.request(server)
            .post(endpoints.addCoffee)
            .send({
                name: config.coffee.name,
                stock: config.coffee.stock,
                price: config.coffee.price,
                intensity: config.coffee.intensity,
            })
            .end((error, res) => {

                res.should.have.status(401);
                res.body.should.be.a('object');
                res.body.should.have.property("errors").be.a("array");
                res.body.errors[0].should.have.property("field").eq("user");
                res.body.errors[0].should.have.property("message").be.a("string");

                done();
            });
    });

    //wrong parameters
    it('should reject adding because of invalid intensity', (done) => {
        chai.request(server)
            .post(endpoints.addCoffee)
            .set({
                "X-access-token": user.auth.token
            })
            .send({
                name: config.coffee.name,
                stock: config.coffee.stock,
                price: config.coffee.price,
                intensity: config.coffee.intensity + 10,
            })
            .end((error, res) => {


                res.should.have.status(406);
                res.body.should.be.a('object');
                res.body.should.have.property("errors").be.a("array");
                res.body.errors[0].should.have.property("field").eq("intensity");
                res.body.errors[0].should.have.property("message").be.a("string");

                done();
            });
    });

    //added successfully
    it('should add coffee successfully', (done) => {
        chai.request(server)
            .post(endpoints.addCoffee)
            .set({
                "X-access-token": user.auth.token
            })
            .send({
                name: config.coffee.name,
                stock: config.coffee.stock,
                price: config.coffee.price,
                intensity: config.coffee.intensity,
            })
            .end((error, res) => {

                res.should.have.status(201);
                res.body.should.be.a('object');
                res.body.should.have.property("status").eq(true);

                done();
            });
    });

});

describe('GET endpoint ' + endpoints.getCoffees, () => {

    //success
    it('should get paginated coffees', (done) => {
        chai.request(server)
            .get(endpoints.getCoffees)
            .set({
                "X-access-token": user.auth.token
            })
            .send({
                page: 1,
                results: 10
            })
            .end((error, res) => {

                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property("results").be.a("array");
                res.body.should.have.property("pages").be.a("number");
                res.body.should.have.property("total").be.a("number");
                res.body.results.should.not.be.empty;
                res.body.results[0].should.have.property("_id").be.a("string");
                res.body.results[0].should.have.property("name").be.a("string");
                res.body.results[0].should.have.property("intensity").be.a("number");
                res.body.results[0].should.have.property("price").be.a("number");
                res.body.results[0].should.have.property("stock").be.a("number");
                res.body.results[0].should.have.property("created").be.a("number");

                done();
            });
    });

});

describe('GET endpoint ' + endpoints.getCoffee, () => {

    let coffee = null;
    before(done => {
        const { Coffee } = require("../app/models/Coffee");
        Coffee.create({
            name: config.coffee.name,
            stock: config.coffee.stock,
            price: config.coffee.price,
            intensity: config.coffee.intensity,
        },
        (err, object) => {
            coffee = object;
            done();
        });
    });

    //not found
    it('should fail because of not found coffee ', (done) => {
        chai.request(server)
            .get(endpoints.getCoffee.replace(/:id/, 1))
            .set({
                "X-access-token": user.auth.token
            })
            .end((error, res) => {

                res.should.have.status(404);
                res.body.should.be.a('object');
                res.body.should.have.property("errors").be.a("array");
                res.body.errors[0].should.have.property("field").eq("coffee");
                res.body.errors[0].should.have.property("message").be.a("string");

                done();
            });
    });

    //success
    it('should get coffee ', (done) => {
        chai.request(server)
            .get(endpoints.getCoffee.replace(/:id/, coffee._id.toString()))
            .set({
                "X-access-token": user.auth.token
            })
            .end((error, res) => {

                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property("_id").be.a("string");
                res.body.should.have.property("name").be.a("string");
                res.body.should.have.property("intensity").be.a("number");
                res.body.should.have.property("price").be.a("number");
                res.body.should.have.property("stock").be.a("number");
                res.body.should.have.property("created").be.a("number");

                done();
            });
    });

});

describe('PUT endpoint ' + endpoints.updateCoffee, () => {

    let coffee = null;
    before(done => {
        const { Coffee } = require("../app/models/Coffee");
        Coffee.create({
                name: config.coffee.name,
                stock: config.coffee.stock,
                price: config.coffee.price,
                intensity: config.coffee.intensity,
            },
            (err, object) => {
                coffee = object;
                done();
            });
    });

    //not found
    it('should fail because of not found coffee ', (done) => {
        chai.request(server)
            .put(endpoints.updateCoffee.replace(/:id/, 1))
            .set({
                "X-access-token": user.auth.token
            })
            .end((error, res) => {

                res.should.have.status(404);
                res.body.should.be.a('object');
                res.body.should.have.property("errors").be.a("array");
                res.body.errors[0].should.have.property("field").eq("coffee");
                res.body.errors[0].should.have.property("message").be.a("string");

                done();
            });
    });

    //success
    it('should update successfully', (done) => {
        chai.request(server)
            .put(endpoints.updateCoffee.replace(/:id/, coffee._id.toString()))
            .set({
                "X-access-token": user.auth.token
            })
            .send({
                name: "Updated",
                stock: coffee.stock + 15,
                price: coffee.price - 2,
                intensity: coffee.intensity - 1
            })
            .end((error, res) => {

                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property("status").eq(true);

                //get object to confirm
                chai.request(server)
                    .get(endpoints.getCoffee.replace(/:id/, coffee._id.toString()))
                    .set({
                        "X-access-token": user.auth.token
                    })
                    .end((error, res) => {

                        res.body.should.have.property("name").eq("Updated");
                        res.body.should.have.property("intensity").eq(coffee.intensity - 1);
                        res.body.should.have.property("price").eq(coffee.price - 2);
                        res.body.should.have.property("stock").eq(coffee.stock + 15);

                        done()
                    })
            });
    });

    //reversing name to clear before another test
    before(done => {
        const { Coffee } = require("../app/models/Coffee");
        Coffee.update({ name: "Updated" }, { $set: { name: config.coffee.name} }, { strict: false, multi: true }, () => done());
    });
});

describe('DELETE endpoint ' + endpoints.deleteCoffee, () => {

    let coffee = null;
    before(done => {
        const { Coffee } = require("../app/models/Coffee");
        Coffee.create({
                name: config.coffee.name,
                stock: config.coffee.stock,
                price: config.coffee.price,
                intensity: config.coffee.intensity,
            },
            (err, object) => {
                coffee = object;
                done();
            });
    });

    //not found
    it('should fail because of not found coffee ', (done) => {
        chai.request(server)
            .delete(endpoints.deleteCoffee.replace(/:id/, 1))
            .set({
                "X-access-token": user.auth.token
            })
            .end((error, res) => {

                res.should.have.status(404);
                res.body.should.be.a('object');
                res.body.should.have.property("errors").be.a("array");
                res.body.errors[0].should.have.property("field").eq("coffee");
                res.body.errors[0].should.have.property("message").be.a("string");

                done();
            });
    });

    //success
    it('should delete successfully', (done) => {
        chai.request(server)
            .put(endpoints.deleteCoffee.replace(/:id/, coffee._id.toString()))
            .set({
                "X-access-token": user.auth.token
            })
            .end((error, res) => {

                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property("status").eq(true);

                done();
            });
    });
});