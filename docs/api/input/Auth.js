
/**
 * @api {post} /register Register
 * @apiSampleRequest /register
 * @apiVersion 0.0.1
 * @apiName Register 
 * @apiGroup Auth
 *
 * @apiParam {String} username Username. RegExp = /^([a-zA-Z0-9_]+)\.?([a-zA-Z0-9_]+)$/.
 * @apiParam {String} password Password. Min 6 characters, RegExp = /^(?=.*[A-Za-z])(?=.*[0-9])[A-Za-z0-9]{6,}$/.

 * @apiSuccess (Success 201){Boolean} status Operation status.
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 201 OK
 *     {
 *       "status" : true,
 *     }
 *
 * @apiError NotAcceptable Wrong parameters.
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 406 Wrong parameters
 *     {
 *          "errors": [
 *              {
 *                   "field": "username",
 *                   "message": "The username has already been taken."
 *              }
 *          ]
 *      }
 */

/**
 * @api {post} /login Login
 * @apiSampleRequest /login
 * @apiVersion 0.0.1
 * @apiName Login
 * @apiGroup Auth
 *
 * @apiParam {String} [username] Username. Required with password.
 * @apiParam {String} [password] User password. Required with username.
 * @apiParam {String} [refresh_token] Token required to handle auth refresh. Required when username and password  has not been sent.
 *
 * @apiSuccess (Success 200){String} token Access token.
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          "_id": "5a5de32f5bb5952104a5d156",
 *          "username": "test.test",
 *          "auth": {
 *              "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1Mzk5MDE5MTQsImRhdGEiOnsiX2lkIjoiNWJiZTc1MDViYjNkM2IxMTU0MDE4OWY5In0sImlhdCI6MTUzOTI5NzExNH0.C3l5ACLcFo_vQw5vhx_Vb3Q6b9_pmwRN50t84R0nu0w",
 *              "refresh_token": "ukbPiozxqppTWWVGoscajlL1m1DceSBAlrkZLGd1HaY2gYSwrs7Dak4mlme0eB6WZHJT8OXg9ajkFMsX2rdmJHrsKphiuA9ho1mpkmVWHINt2GxSU8oUXlvkYFnunH4u"
 *          }
 *     }
 *
 * @apiError AuthorizationFailed Authorization failed.
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 401 Authorization failed
 *     {
 *          "errors": [
 *              {
 *                   "field": "auth",
 *                   "message": "Authorization failed"
 *              }
 *          ]
 *      }
 */