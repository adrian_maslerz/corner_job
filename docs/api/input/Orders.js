/**
 * @api {post} /coffees/:id/orders Add order
 * @apiSampleRequest off
 * @apiVersion 0.0.1
 * @apiName Add order
 * @apiGroup Order
 *
 * @apiHeader {String} X-access-token Access token
 * @apiParam {String} id Coffee id.
 * @apiParam {Number} [quantity] Order quantity. Min 1
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 201 OK
 *     {
 *          "status": true
 *     }
 *
 * @apiError AccessDenied Access Denied.
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 401 Access Denied
 *     {
 *          "errors": [
 *              {
 *                   "field": "user",
 *                   "message": "Access Denied"
 *              }
 *          ]
 *      }
 *
 * @apiError NotFound Not found.
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not found
 *     {
 *          "errors": [
 *              {
 *                   "field": "coffee",
 *                   "message": "Coffee not found"
 *              }
 *          ]
 *      }
 *
 * @apiError WrongParameters Wrong parameters.
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 406 Wrong Parameters
 *     {
 *          "errors": [
 *              {
 *                   "field": "name",
 *                   "message": "Name is required"
 *              }
 *          ]
 *      }
 *
 * @apiError Conflict Conflict.
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 409 Conflict
 *     {
 *          "errors": [
 *              {
 *                   "field": "stock",
 *                   "message": "Coffee out of stock"
 *              }
 *          ]
 *      }
 *
 * @apiError ExpiredToken Token expired.
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 410 Token expired
 *     {
 *          "errors": [
 *              {
 *                   "field": "token",
 *                   "message": "Access token expired"
 *              }
 *          ]
 *      }
 */

/**
 * @api {get} /orders/:id Get orders
 * @apiSampleRequest off
 * @apiVersion 0.0.1
 * @apiName Get orders
 * @apiGroup Order
 *
 * @apiHeader {String} X-access-token Access token
 * @apiParam {Number} [page] Page number.
 * @apiParam {Number} [results] Page results. Default 10.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          "results": [
 *              {
 *                  "_id": "5bbfc83137b4313ef8894308",
 *                  "user": {
 *                      "_id": "5bbe7505bb3d3b11540189f9",
 *                      "username": "test.test"
 *                  },
 *                  "coffee": {
 *                      "_id": "5bbe85f8099d342318085d6e",
 *                      "name": "Test coffee",
 *                      "intensity": 10,
 *                      "price": 20,
 *                      "stock": 6
 *                  },
 *                  "quantity": 8,
 *                  "created": 1539295281676,
 *                  "amount": 160
 *              }
 *
 *          ],
 *          "pages": 1,
 *          "total": 3
 *     }
 *
 * @apiError AccessDenied Access Denied.
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 401 Access Denied
 *     {
 *          "errors": [
 *              {
 *                   "field": "user",
 *                   "message": "Access Denied"
 *              }
 *          ]
 *      }
 *
 * @apiError PermissionDenied Permission Denied.
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 403 Permission Denied
 *     {
 *          "errors": [
 *              {
 *                   "field": "permission",
 *                   "message": "Permission Denied"
 *              }
 *          ]
 *      }
 *
 * @apiError ExpiredToken Token expired.
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 410 Token expired
 *     {
 *          "errors": [
 *              {
 *                   "field": "token",
 *                   "message": "Access token expired"
 *              }
 *          ]
 *      }
 */