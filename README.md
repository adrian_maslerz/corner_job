Corner Coffee Marketplace

INSTALL AND RUN with Docker

1. Install Docker
2. Run docker-compose up --build to run all containers and build Dockerfile image
3. App will start on port 8000 (make sure this port is not taken by other process)
3. At the address [your host]:8000/api/documentation you will find documentation of all endpoints.