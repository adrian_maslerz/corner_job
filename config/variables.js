//loading environment config
const env = process.env;

module.exports = {
	database: {
		host: env.DB_HOST || "localhost",
		port: env.DB_PORT || 27017,
		user: env.DB_USER || "",
		password: env.DB_PASSWORD || "",
		database: env.DB_DATABASE || "BaseApi"
	},
	folders: {
		views: __dirname + "/../views",
		public: __dirname + "/../public",
	},
	logs: {
		logfile: __dirname + "/../logs.log"
	}
}