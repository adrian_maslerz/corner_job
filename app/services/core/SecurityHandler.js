//core
const JWT = require('jsonwebtoken');
const async = require('async');

//custom
const { User, statuses: userStatuses } = require("../../models/User");
const { roles} = require('../../../config/access-control');

module.exports = {
	authenticate: function(token)
	{
		return new Promise((resolve, reject) => {

			async.waterfall([

				//checking if token is assigned to user
				(callback) => {
					User.findOne({ "auth.token": token }, (error, user) => {

						if(!user)
							return reject({ error: "Access Denied", field: "user", status: 401 });

						callback(null, user)
					});
				},

				//token verification
				(user, callback) => {

					JWT.verify(token, user.auth.signature_key , (error) => {
						//error handle
						if(error)
						{
							if(error.name == "TokenExpiredError")
								return reject({ error: "Access token expired", field: "user", status: 410 });

							return reject({ error: "Access Denied", field: "user", status: 401 });
						}

						callback(null, user);
					});
				},

			], (error, user) => resolve(user));
		});
	},


	verifyPermissions: function(user, requiredPermissions)
	{
		let isPermitted = false;
		let validPermissions = [];

		//permissions handle
		user.permissions.forEach(permission => {
			if (requiredPermissions.indexOf(permission) != -1 && validPermissions.indexOf(permission) == -1)
				validPermissions.push(permission);
		});

		//role permissions handle
		user.roles.forEach(role => {
			const configRole = roles.find(configRole => configRole.role == role);
			const rolePermissions = configRole ? configRole.permissions : [];

			rolePermissions.forEach(permission => {
				if (requiredPermissions.indexOf(permission) != -1 && validPermissions.indexOf(permission) == -1)
					validPermissions.push(permission);
			});
		});

		if(validPermissions.length >= requiredPermissions.length)
			isPermitted = true;

		return isPermitted;
	}
}