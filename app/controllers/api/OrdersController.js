
//models
const { Order } = require("../../models/Order");
const { Coffee } = require("../../models/Coffee");

//services
const Utils = require('../../services/utilities');

module.exports = class OrdersController
{
    async addOrder(req, res)
    {
        //getting coffee
        const coffee = await Coffee.findById(req.params.id).catch(error => console.log(error));

        //not found
        if(!coffee)
            return res.status(404).json(Utils.parseStringError("Coffee not found", "coffee"));

        //new order
        const order = new Order({
            user: req.user,
            coffee: coffee,
            quantity: req.body.quantity
        });

        //checking coffee stock
        if(coffee.stock - order.quantity < 0)
            return res.status(409).json(Utils.parseStringError("Coffee out of stock", "stock"));

        //validation and saving
        order
            .save()
            .then(() => res.status(201).json({ status: true }))
            .catch(error => res.status(406).json(Utils.parseValidatorErrors(error)));
    }

    async getOrders(req, res)
    {
        const options = {
            select: { amount: 1, quantity: 1, created: 1 },
            sort: { created: -1 },
            populate: [
                {
                    path: "user",
                    select: { username: 1 }
                },
                {
                    path: "coffee",
                    select: { name: 1, intensity: 1, price: 1, stock: 1 }
                }
            ],
            lean: true,
            leanWithId: false
        };

        const query = { };

        //pagination and sending response
        const paginated = await Utils.paginate(Order, { query: query, options: options }, req);
        paginated.results.map(post => Order.parse(post, req));
        res.json(paginated);
    }
}

