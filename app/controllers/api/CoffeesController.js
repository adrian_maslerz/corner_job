
//models
const { Coffee } = require("../../models/Coffee");

//services
const Utils = require('../../services/utilities');

module.exports = class CoffeesController
{
    addCoffee(req, res)
    {
        //new coffee
        const coffee = new Coffee({
            name: req.body.name,
            intensity: req.body.intensity,
            price: req.body.price,
            stock: req.body.stock
        });

        //validation and saving
        coffee
            .save()
            .then(() => res.status(201).json({ status: true }))
            .catch(error => res.status(406).json(Utils.parseValidatorErrors(error)));
    }

    async getCoffees(req, res)
    {
        const options = {
            select: { name: 1, intensity: 1, price: 1, stock: 1, created: 1 },
            sort: { created: -1 },
            lean: true,
            leanWithId: false
        };

        const query = { };

        //pagination and sending response
        const paginated = await Utils.paginate(Coffee, { query: query, options: options }, req);
        paginated.results.map(post => Coffee.parse(post, req));
        res.json(paginated);
    }

    async getCoffee(req, res)
    {
        //getting coffee
        const coffee = await Coffee.findById(req.params.id, { name: 1, intensity: 1, price: 1, stock: 1, created: 1 }).lean().catch(error => console.log(error));

        //not found
        if(!coffee)
            return res.status(404).json(Utils.parseStringError("Coffee not found", "coffee"));

        //sending response
        res.json(Coffee.parse(coffee, req));
    }

    async updateCoffee(req, res)
    {
        //getting coffee
        const coffee = await Coffee.findById(req.params.id).catch(error => console.log(error));

        //not found
        if(!coffee)
            return res.status(404).json(Utils.parseStringError("Coffee not found", "coffee"));

        //updating values
        coffee.set({
            name: req.body.name || coffee.name,
            intensity: req.body.intensity || coffee.intensity,
            price: req.body.price || coffee.price,
            stock: req.body.stock || coffee.stock
        })

        //validation and saving
        coffee
            .save()
            .then(() => res.json({ status: true }))
            .catch(error => res.status(406).json(Utils.parseValidatorErrors(error)));
    }

    async deleteCoffee(req, res)
    {
        //getting coffee
        const coffee = await Coffee.findById(req.params.id).catch(error => console.log(error));

        //not found
        if(!coffee)
            return res.status(404).json(Utils.parseStringError("Coffee not found", "coffee"));

        //removing coffee
        coffee.remove();

        //sending response
        res.json({ status: true });
    }
}

