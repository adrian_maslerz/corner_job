//core
const bcrypt = require('bcrypt-nodejs');

//models
const { User } = require("../../models/User");

//services
const Utils = require('../../services/utilities');

module.exports = class AuthController
{
	async register(req, res)
	{
	    //building user object
	    let user = new User({
		    username: req.body.username,
		    password: req.body.password,
		    roles: ["USER"]
	    });

	    //validation
	    user
		    .validate()
		    .then(() => {

			    //sending response
                user.generateAccessTokens();
			    res.status(201).json(getLoginUser(user));

			    //hashing password and saving user
			    bcrypt.hash(user.password, null, null, (error, hash) => {
				    user.password = hash;
				    user.save().catch(error => console.log(error));
			    });
		    })
		    .catch(error => res.status(406).json(Utils.parseValidatorErrors(error)));
	}

	async login (req, res)
    {
        let query = {};

	    //preparing query
	    if (req.body.username && req.body.password)
		    query[ "username" ] = req.body.username;
	    else if (req.body.refresh_token)
		    query[ "auth.refresh_token" ] = req.body.refresh_token;
	    else
		    return res.status(401).json(Utils.parseStringError("Authorization failed", "auth"));

	    //getting user
	    const user = await User.findOne(query).exec();
        if (!user)
            return res.status(401).json(Utils.parseStringError("Authorization failed", "auth"));

	    //refresh token login handle
        if (query[ "auth.refresh_token" ])
        {
            user.generateAccessTokens();
            user.save().catch(error => console.log(error));

            return res.json({ user: getLoginUser(user) });
        }

        //password login
	    else
        {
            bcrypt.compare(req.body.password, user.password, (error, status) => {

                if (!status)
                    return res.status(401).json(Utils.parseStringError("Authorization failed", "auth"));

                user.generateAccessTokens();
                user.save().catch(error => console.log(error));

	            return res.json(getLoginUser(user));
            });
        }
    }
}

function getLoginUser(user)
{
    return {
        _id: user._id,
        username: user.username,
        auth: {
            token: user.auth.token,
            refresh_token: user.auth.refresh_token
        }
    }
}