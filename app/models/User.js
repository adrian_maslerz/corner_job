//modules
const randomstring = require("randomstring");
const mongoose = require('mongoose');
const JWT = require('jsonwebtoken');
const uniqueValidator = require('mongoose-unique-validator');
const mongoosePaginate = require('mongoose-paginate');
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');

//custom
const validators = require('./../services/validators');
const { permissions, roles } = require("../../config/access-control");

//settings
const accessTokenExpirationDays = 7;
const passwordRegExp = /^(?=.*[A-Za-z])(?=.*[0-9])[A-Za-z0-9]{6,}$/;
const usernameRegExp = /^([a-zA-Z0-9_]+)\.?([a-zA-Z0-9_]+)$/;

//schema
const schema = mongoose.Schema({
    username: {
        type: String,
        required: [ true, "{PATH} is required." ],
        validate: [ validators.username(usernameRegExp), validators.filled ],
        minlength: [ 6, "{PATH} should have at least {MINLENGTH} characters." ],
        trim: true,
        unique: true
    },
    password: {
        type: String,
        required: [ true, "{PATH} is required." ],
        validate: validators.password(passwordRegExp, "{PATH} must have at least 6 characters and contain at least one letter and number."),
        minlength: [ 6, "{PATH} should have at least {MINLENGTH} characters." ],
    },
	auth: {
		token: {
			type: String,
			default: null
		},
		refresh_token: {
			type: String,
			default: null
		},
		signature_key: {
			type: String,
			default: null
		},
	},
	roles: [
		{
			type: String,
			required: true,
			enum: roles.map(role => role.role)
		}
	],
	permissions: [
		{
			type: String,
			required: true,
			enum: permissions
		}
	],
	orders: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Order",
        }
	],
	created: {
		type: Date,
		default: Date.now()
	},
	updated: {
		type: Date,
		default: Date.now()
	}
}, { usePushEach: true });

//middlewares
schema.pre("save", function (next)
{
	const user = this;

    //dates handle
    user.updated = new Date();
    if(user.isNew)
        user.created = new Date();

    next();
});

schema.post("remove", async function (user)
{

    //removing all connected orders
    const { Order } = require("./Order");
    const orders = await Order.find({ user: user._id }).exec();

    await Promise.all(orders.mac(async order => await order.remove()))
});

//methods
schema.methods.generateAccessTokens = function ()
{
	//refreshes access tokens
	const key = randomstring.generate(128);
	this.auth = {
		signature_key: key,
		refresh_token: randomstring.generate(128),
		token: JWT.sign({
			exp: Math.floor(Date.now() / 1000) + (60 * 60 * 24 * accessTokenExpirationDays),
			data: {
				_id: this._id,
				email: this.email,
				first_name: this.first_name,
				last_name: this.last_name
			}
		}, key)
	}

	return this.auth;
}

//statics
schema.statics.parse = function(user, req)
{
    return user;
}


schema.plugin(uniqueValidator, { message: 'The {PATH} has already been taken.' });
schema.plugin(mongoosePaginate);
schema.plugin(mongooseAggregatePaginate);

module.exports = { User: mongoose.model("User", schema) }