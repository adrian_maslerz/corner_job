//modules
const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');

//custom
const validators = require('./../services/validators');

//schema
const schema = mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
        required: [ true, "{PATH} is required." ]
    },
    coffee: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Coffee",
        required: [ true, "{PATH} is required." ]
    },
    amount: {
        type: Number,
        default: 0,
    },
    quantity: {
        type: Number,
        required: [ true, "{PATH} is required." ],
        validate: validators.integer,
        min: [1, "{PATH} cannot be lower than {MIN}."]
    },
	created: {
		type: Date,
		default: Date.now()
	},
	updated: {
		type: Date,
		default: Date.now()
	}
}, { usePushEach: true });

//middlewares
schema.pre("save", async function (next)
{
	const order = this;

    //dates handle
    order.updated = new Date();
    if(order.isNew)
        order.created = new Date();

    //amount calculation
    if(order.isNew)
        order.amount = order.coffee.price * order.quantity;

    //adding references
    if(order.isNew)
    {
        //for user
        const { User } = require("./User");
        const user = await User.findById(order.user).catch(error => console.log(error));
        user.orders.push(order);
        await user.save().catch(error => console.log(error));

        //for coffee + reducing stock
        const { Coffee } = require("./Coffee");
        const coffee = await Coffee.findById(order.coffee).catch(error => console.log(error));
        coffee.stock = coffee.stock - order.quantity;
        coffee.orders.push(order);
        await coffee.save().catch(error => console.log(error));
    }

    //logs
    const variables = require('./../../config/variables');
    const logger = require('logger').createLogger(variables.logs.logfile);

    if(order.isNew)
        logger.info("SET ORDER -> " + JSON.stringify(order));

    next();
});

schema.post("remove", async function (order)
{

    //for user
    const { User } = require("./User");
    const user = await User.findById(order.user).catch(error => console.log(error));
    if(user)
    {
        user.orders.pull(order);
        await user.save().catch(error => console.log(error));
    }

    //for coffee
    const { Coffee } = require("./Coffee");
    const coffee = await Coffee.findById(order.coffee).catch(error => console.log(error));
    if(coffee)
    {
        coffee.orders.pull(order);
        await coffee.save().catch(error => console.log(error));
    }
});

//statics
schema.statics.parse = function(order, req)
{
    if(order.created)
        order.created = order.created.getTime();

    return order;
}

schema.plugin(mongoosePaginate);
schema.plugin(mongooseAggregatePaginate);

module.exports = { Order: mongoose.model("Order", schema) }