//modules
const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');

//custom
const validators = require('./../services/validators');


//schema
const schema = mongoose.Schema({
    name: {
        type: String,
        required: [ true, "{PATH} is required." ],
        maxlength: [ 50, "{PATH} can't have more than {MAXLENGTH} characters." ],
        trim: true
    },
    intensity: {
        type: Number,
        required: [ true, "{PATH} is required." ],
        validate: validators.integer,
        min: [1, "{PATH} cannot be lower than {MIN}."],
        max: [10, "{PATH} cannot be greater than {MAX}."],
    },
    price: {
        type: Number,
        required: [ true, "{PATH} is required." ],
        min: [0, "{PATH} cannot be lower than {MIN}."]
    },
    stock: {
        type: Number,
        required: [ true, "{PATH} is required." ],
        validate: validators.integer,
        min: [0, "{PATH} cannot be lower than {MIN}."]
    },
	orders: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Order",
        }
	],
	created: {
		type: Date,
		default: Date.now()
	},
	updated: {
		type: Date,
		default: Date.now()
	}
}, { usePushEach: true });

//middlewares
schema.pre("save", function (next)
{
	const coffee = this;

    //dates handle
    coffee.updated = new Date();
    if(coffee.isNew)
        coffee.created = new Date();

    //logs
    const variables = require('./../../config/variables');
    const logger = require('logger').createLogger(variables.logs.logfile);

    if(coffee.isNew)
        logger.info("SET COFFEE -> " + JSON.stringify(coffee));
    else
    {
        const modified = {};
        coffee.modifiedPaths().forEach(path => {
            modified[path] = coffee[path];
        })
        logger.info("UPDATE COFFEE " + coffee._id.toString() +" -> modifications " + JSON.stringify(modified));
    }

    next();
});

schema.post("remove", async function (coffee)
{

    //removing all connected orders
    const { Order } = require("./Order");
    const orders = await Order.find({ coffee: coffee._id }).exec();

    await Promise.all(orders.mac(async order => await order.remove()))
});

//statics
schema.statics.parse = function(coffee, req)
{
    if(coffee.created)
        coffee.created = coffee.created.getTime();

    return coffee;
}

schema.plugin(mongoosePaginate);
schema.plugin(mongooseAggregatePaginate);

module.exports = { Coffee: mongoose.model("Coffee", schema) }