FROM node:8

#create workdir
ARG APP_DIR=/usr/src/app
WORKDIR $APP_DIR

#install grunt
RUN npm install -g grunt-cli

#install dependencies
COPY package*.json $APP_DIR/
RUN npm install

#copy source
COPY app $APP_DIR/app
COPY bin $APP_DIR/bin
COPY config $APP_DIR/config
COPY docs $APP_DIR/docs
COPY public $APP_DIR/public
COPY routes $APP_DIR/routes
COPY views $APP_DIR/views
COPY .env $APP_DIR/
COPY server.js $APP_DIR/
COPY Gruntfile.js $APP_DIR/

RUN grunt apidoc

CMD [ "npm", "start" ]
