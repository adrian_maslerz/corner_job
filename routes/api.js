//
const AppRouter = require('../app/services/core/AppRouter');

//controllers
const AuthController = require('../app/controllers/api/AuthController');
const CoffeesController = require('../app/controllers/api/CoffeesController');
const OrdersController = require('../app/controllers/api/OrdersController');

//middlewares
const authenticate = require('../app/middlewares/authenticate');
const security = require('../app/middlewares/security');

const path = "/api";
const routes = [

	//Auth
	{
		path: "/", controller: AuthController, children: [
			{ path: "/register", method: "post", function: "register"},
			{ path: "/login", method: "post", function: "login"}
		]
	},

	//Coffees
	{
		path: "/coffees", controller: CoffeesController, middlewares:[ authenticate ], children: [
			{ path: "/", method: "post", function: "addCoffee", middlewares:[ security(["ADMIN_FULL"]) ] },
			{ path: "/", method: "get", function: "getCoffees" },
			{ path: "/:id", method: "get", function: "getCoffee" },
			{ path: "/:id", method: "put", function: "updateCoffee", middlewares:[ security(["ADMIN_FULL"]) ] },
			{ path: "/:id", method: "delete", function: "deleteCoffee", middlewares:[ security(["ADMIN_FULL"]) ] },

			//Orders
            { path: "/:id/orders", controller: OrdersController, method: "post", function: "addOrder" }
		]
	},

    //Orders
    {
        path: "/orders", controller: OrdersController, middlewares:[ authenticate, security(["ADMIN_FULL"]) ], children: [
            { path: "/", method: "get", function: "getOrders" },
        ]
    },
];

module.exports.load = function(app)
{
	//create routes
	let appRouter = new AppRouter();
	appRouter.create(routes);

	//documentation
	appRouter.registerDocumentation(path);

	app.use(path, appRouter.router);
}


